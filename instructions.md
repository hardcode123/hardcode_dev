Инструкция по установке веб-сервера
1. Обновить список пакетов: sudo apt update

2. Установить Nginx: sudo apt install nginx

3. Запустить Nginx: sudo service nginx start

4. Проверить статус: sudo service nginx status
